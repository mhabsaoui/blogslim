<?php
// Autoloader inclusion
require_once 'vendor/autoload.php';

// DB configuration
Configuration::config();

// Slim app creation
$app = new \Slim\Slim(array('templates.path' => 'app/view'));

// Do not forget sessions...
session_start ();

//
// Now routing definition
//

/* Get routes */

$app->get('/', function() use ($app) {
    $c = new AnonymousController($app);
    $c->index();
})->name('root');

$app->get('/item/:id', function($id) use ($app) {
    $c = new AnonymousController($app);
    $c->affiche_item($id);
})->name('affitem');

$app->get('/inscription', function() use ($app) {
    $c = new AnonymousController($app);
    $c->inscrire();
})->name('inscription');

$app->get('/connection', function() use ($app) {
    $c = new AnonymousController($app);
    $c->connecter();
})->name('connection');

$app->get('/deconnection', function() use ($app) {
    $c = new AnonymousController($app);
    $c->deconnecter();
})->name('deconnection');

$app->get('/admin', function() use ($app) {
    $c = new AnonymousController($app);
    $c->administrer();
})->name('admin');

$app->get('/affItemsByCategorie/:id', function($id) use ($app) {
    $c = new AnonymousController($app);
    $c->affiche_items_by_categorie($id);
})->name('affItemsByCategorie');

$app->get('/saisie_billet', function() use ($app) {
    $c = new AnonymousController($app);
    $c->saisie_billet();
})->name('saisie_billet');


/* Post routes */

$app->post('/ajout_membre', function() use ($app) {
    $c = new AnonymousController($app);
    $c->insert_membre();
});

$app->post('/connect_membre', function() use ($app) {
    $c = new AnonymousController($app);
    $c->connect_membre();
});

$app->post('/admin', function() use ($app) {
    $c = new AnonymousController($app);
    $c->insert_categorie();
});

$app->post('/saisie_billet', function() use ($app) {
    $c = new AnonymousController($app);
    $c->insert_billet();
});

/* Finally, generate result */
$app->run();

?>