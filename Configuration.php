<!-- Create your own configuration.php file based on this skeleton -->
<!-- And use a *ConnectionFactory*, based on an .ini file!! -->

<?php
use Illuminate\Database\Capsule\Manager as DB;

class Configuration{
	public static function config() {
		$db = new DB();
		
		$db->addConnection(array(
				'driver' => 'mysql',
				'host'  => 'localhost',
				'database' => 'habsaoui2u',
				'username' => 'habsaoui2u',
				'password' => 'habsaoui2u',
				'charset' => 'utf8',
				'collation' => 'utf8_unicode_ci',
				'prefix' => ''
		));
		$db->setAsGlobal();
		$db->bootEloquent();
	}
	
	static function baseURL() {
		// Ici, il faut renvoyer la même chaîne
		// que celle déclarée dans le .htaccess au
		// niveau de la directive RewriteBase
		// (normalement /~login/repertoire)
		// (renvoyer la chaîne vide si rien n'a été défini
		// en regard de cette directive).
		return "https://webetu.iutnc.univ-lorraine.fr/~habsaoui2u/blogslim/";
	}
}
?>
