<?php

class User extends \Illuminate\Database\Eloquent\Model {
	protected $table = 'users';
	protected $primaryKey = 'id_user';
	public $timestamps = false;

	function createUser($login, $password){
		$user = User::where('login', $login)->get();
		if ($user->isEmpty()){
			$user = new User;
			$user->login = $login;
			$user->password = sha1($password);
			$user->save();
			//mettre les infos en session
		    $_SESSION['id'] = $user->id_user;
			$_SESSION['login'] = $user->login;
			$_SESSION['droit_admin'] = $user->droit_admin;
			return $user->id_user;
		}
		return 0;
	}

	function connectUser($login, $password){
		$users = User::where('login', $login)->take(1)->get();
		if(!($users->isEmpty())){
			$user = $users[0];
			if( $user->password == sha1($password) ){
				$_SESSION['id'] = $user->id_user;
				$_SESSION['login'] = $user->login;
				$_SESSION['droit_admin'] = $user->droit_admin;
				return $user->id_user;
			}
			else
				return 0;
		}
		else
			return 0;
	}

	function disconnectUser(){
		$_SESSION = array();
		session_destroy();
	}
}

?>