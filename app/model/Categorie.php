<?php

class Categorie extends \Illuminate\Database\Eloquent\Model {
	protected $table = 'categories';
	protected $primaryKey = 'id_categorie';
	public $timestamps = false;

	static function getAllCategories(){
		return Categorie::all();//->orderBy('id_categorie')->get();
	}

	static function createCategorie($nom){
		$cat = Categorie::where('nom', $nom)->get();
		if($cat->isEmpty()){
			$cat = new Categorie;
			$cat->nom = $nom;
			$cat->save();	
			return $cat->id_categorie;		
		}
		return 0;
	}
}

?>