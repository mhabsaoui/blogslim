<?php

class AnonymousController extends Controller {

	public function header() {
		$app = $this->app;
		$app->render('header.php',compact('app'));
	}

	public function banniere(){
		$this->app->render('banniere.php');
	}

	public function footer() {
		$this->app->render('footer.php');
	}

	public function index(){
		$billets = Billets::get10BilletsOrderedByDate();
		$categories = Categorie::getAllCategories();
		$this->header();
		$this->banniere();
		$this->app->render('homepage.php', compact('billets', 'categories'));
		$this->footer();
	}

	public function affiche_items_by_categorie($id){
		$this->header();
		$this->banniere();
		$categories = Categorie::getAllCategories();
		$billets = Billets::getFromCategorie($id);
		$current = Categorie::find($id);
		$this->app->render('aff_items_by_categorie.php', compact('billets', 'current', 'categories'));
		$this->footer();
	}

	public function saisie_billet(){
		$categories = Categorie::getAllCategories();
		$this->header();
		$this->banniere();
		$this->app->render('saisie_billet.php', compact('categories'));
		$this->footer();
	}

	public function affiche_item($id){
		$billet = Billets::getDetailsBillets($id);
		$this->header();
		$this->banniere();
		$this->app->render('aff_item.php', compact('id', 'billet'));
		$this->footer();
	}

	public function inscrire(){
		$this->header();
		$this->banniere();
		$this->app->render('inscription.php');
		$this->footer();
	}

	public function connecter(){
		$this->header();
		$this->banniere();
		$this->app->render('connection.php');
		$this->footer();
	}

	public function deconnecter(){
		User::disconnectUser();
		$this->app->redirect($this->app->urlFor('root'));	
	}

	public function administrer(){
			$this->header();
			$this->banniere();
			$categories = Categorie::getAllCategories();
			if (isset($_SESSION["login"]) && $_SESSION['droit_admin']==1)
				$this->app->render('admin.php', compact('categories'));
			else
				$this->app->redirect($this->app->urlFor('root'));	
			$this->footer();
	}

	// insertion membre
	public function insert_membre(){
		$login = $this->app->request->post('login');
		$password = $this->app->request->post('password');
		$ok = User::createUser($login, $password);
		// si membre déjà existant ou erreur de saisie => rediriger vers inscription
		if ($ok == 0)
			$this->app->redirect($this->app->urlFor('inscription'));
		// sinon membre crée et rediriger vers accueil 
		else
			$this->app->redirect($this->app->urlFor('root'));

		// $fname   = isset($_POST["fname"]) ? mysql_real_escape_string($_POST['fname']) : '';
		// $lname   = isset($_POST['lname']) ? mysql_real_escape_string($_POST['lname']) : '';
		// $email   = isset($_POST['email']) ? mysql_real_escape_string($_POST['email']) : '';
		// you might also want to validate e-mail:
		// if($mail = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
		// {
		//   $email = mysql_real_escape_string($_POST['email']);
		// }
		// else
		// {
		//   //die ('invalid email address');
		//   // or whatever, a default value? $email = '';
		// }
		// $paswod  = isset($_POST["paswod"]) ? mysql_real_escape_string($_POST['paswod']) : '';
		// $gender  = isset($_POST['gender']) ? mysql_real_escape_string($_POST['gender']) : '';

		// $query = mysql_query("SELECT Email FROM users WHERE Email = '".$email."')";
		// if(mysql_num_rows($query)> 0)
		// {
		//   echo 'userid is already there';
		// }
		// else
		// {
		//  $sql = "INSERT INTO users (FirstName, LastName, Email, Password, Gender)
		//          VALUES ('".$fname."','".$lname."','".$email."','".paswod."','".$gender."')";
		// $res = mysql_query($sql) or die('Error:'.mysql_error());
		// echo 'created';
	}

	// connection membre
	public function connect_membre(){
		$login = $this->app->request->post('login');
		$password = $this->app->request->post('password');
		$ok = User::connectUser($login, $password);
		// si erreur de login/pass => rediriger vers connexion
		if(!$ok)
			$this->app->redirect($this->app->urlFor('connection'));
		// sinon membre connecté et rediriger vers accueil
		else{
			if($_SESSION['droit_admin']==1)
				$this->app->redirect($this->app->urlFor('admin'));	
			else
				$this->app->redirect($this->app->urlFor('root'));			
		}
	}

	// Insertion nouvelle catégorie
	public function insert_categorie(){
		$cat = $this->app->request->post('categorie');
		$ok = Categorie::createCategorie($cat);
		$categories = Categorie::getAllCategories();
		$this->header();
		$this->banniere();
		$this->app->render('admin.php', compact('ok', 'categories'));
		$this->footer();
	}

	// insertion nouveau billet
	public function insert_billet(){
		$titre = $this->app->request->post('titre');
		$comment = $this->app->request->post('body');
		$user = $_SESSION['id'];
		$categorie = $this->app->request->post('categorie');
		Billets::createBillet($titre, $comment, $user, $categorie);
		$billets = Billets::get10BilletsOrderedByDate();
		$this->app->redirect($this->app->urlFor('root'));
	}
}

