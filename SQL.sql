-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:8889
-- Généré le :  Lun 11 Mai 2015 à 10:33
-- Version du serveur :  5.5.38
-- Version de PHP :  5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `blogslim`
--

-- --------------------------------------------------------

--
-- Structure de la table `billets`
--

CREATE TABLE `billets` (
`id_billet` int(10) NOT NULL,
  `categorie` int(11) DEFAULT '1',
  `titre` varchar(64) NOT NULL,
  `body` text,
  `date` datetime NOT NULL,
  `id_user` int(10) NOT NULL COMMENT 'user qui a créé le billet'
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `billets`
--

INSERT INTO `billets` (`id_billet`, `categorie`, `titre`, `body`, `date`, `id_user`) VALUES
(1, 1, 'billet test 1', 'Généralement, on utilise un texte en faux latin (le texte ne veut rien dire, il a été modifié), le Lorem ipsum ou Lipsum, qui permet donc de faire office de texte d''attente. L''avantage de le mettre en latin est que l''opérateur sait au premier coup d''oeil que la page contenant ces lignes n''est pas valide, et surtout l''attention du client n''est pas dérangée par le contenu, il demeure concentré seulement sur l''aspect graphique.', '2015-04-28 00:00:00', 1),
(4, 2, 'billet test 4', 'Ce texte a pour autre avantage d''utiliser des mots de longueur variable, essayant de simuler une occupation normale. La méthode simpliste consistant à copier-coller un court texte plusieurs fois (« ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ») a l''inconvénient de ne pas permettre une juste appréciation typographique du résultat final.', '2015-04-30 00:00:00', 1),
(3, 3, 'billet test 3', 'Il circule des centaines de versions différentes du Lorem ipsum, mais ce texte aurait originellement été tiré de l''ouvrage de Cicéron, De Finibus Bonorum et Malorum (Liber Primus, 32), texte populaire à cette époque, dont l''une des premières phrases est : « Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... » (« Il n''existe personne qui aime la souffrance pour elle-même, ni qui la recherche ni qui la veuille pour ce qu''elle est... »).', '2015-04-29 12:12:00', 2),
(5, 2, 'billet test 5', 'Expert en utilisabilité des sites web et des logiciels, Jakob Nielsen souligne que l''une des limites de l''utilisation du faux-texte dans la conception de sites web est que ce texte n''étant jamais lu, il ne permet pas de vérifier sa lisibilité effective. La lecture à l''écran étant plus difficile, cet aspect est pourtant essentiel. Nielsen préconise donc l''utilisation de textes représentatifs plutôt que du lorem ipsum. On peut aussi faire remarquer que les formules conçues avec du faux-texte ont tendance à sous-estimer l''espace nécessaire à une titraille immédiatement intelligible, ce qui oblige les rédactions à formuler ensuite des titres simplificateurs, voire inexacts, pour ne pas dépasser l''espace imparti.', '2015-04-30 00:00:00', 2),
(6, 1, 'billet test 6', 'Contrairement à une idée répandue, le faux-texte ne donne même pas un aperçu réaliste du gris typographique, en particulier dans le cas des textes justifiés : en effet, les mots fictifs employés dans le faux-texte ne faisant évidemment pas partie des dictionnaires des logiciels de PAO, les programmes de césure ne peuvent pas effectuer leur travail habituel sur de tels textes. Par conséquent, l''interlettrage du faux-texte sera toujours quelque peu supérieur à ce qu''il aurait été avec un texte réel, qui présentera donc un aspect plus sombre et moins lisible que le faux-texte avec lequel le graphiste a effectué ses essais. Un vrai texte pose aussi des problèmes de lisibilité spécifiques (noms propres, numéros de téléphone, retours à la ligne fréquents, composition des citations en italiques, intertitres de plus de deux lignes...) qu''on n''observe jamais dans le faux-texte.', '2015-04-30 00:00:00', 2),
(7, 3, 'billet test 7', 'ceci est le billet de test créé sur PHPMYADMIN', '2015-04-30 00:00:00', 2),
(8, 2, 'billet test 8', 'ceci est le billet de test créé sur PHPMYADMIN', '2015-04-30 00:00:00', 2),
(9, 1, 'billet test 9', 'ceci est le billet de test créé sur PHPMYADMIN', '2015-04-30 00:00:00', 2),
(10, 1, 'billet test 10', 'ceci est le billet de test créé sur PHPMYADMIN', '2015-04-30 00:00:00', 3),
(11, 2, 'billet test 11', 'ceci est le billet de test créé sur PHPMYADMIN', '2015-04-30 00:00:00', 2),
(12, 3, 'billet test 12', 'ceci est le billet de test créé sur PHPMYADMIN', '2015-04-30 00:00:00', 2),
(13, 1, 'billet test 13', 'ceci est le billet de test créé sur PHPMYADMIN', '2015-05-01 09:28:00', 2),
(14, 3, 'billet test 14', 'ceci est le billet de test créé sur PHPMYADMIN', '2015-05-02 13:23:00', 2),
(15, 2, 'billet test 15', 'ceci est le billet de test créé sur PHPMYADMIN', '2015-05-03 00:00:00', 2),
(2, 1, 'billet de test n° 2', 'bla bla bla', '2015-04-29 00:00:00', 3);

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `nom` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`id`, `nom`) VALUES
(1, 'divers'),
(2, 'categorie 2'),
(3, 'catégorie 3');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
`id_user` int(10) NOT NULL,
  `login` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `droit_admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id_user`, `login`, `password`, `droit_admin`) VALUES
(1, 'admin', 'admin', 1),
(2, 'user1', 'user1', 0),
(3, 'user2', 'user2', 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `billets`
--
ALTER TABLE `billets`
 ADD PRIMARY KEY (`id_billet`), ADD UNIQUE KEY `id` (`id_billet`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id_user`), ADD UNIQUE KEY `id_user` (`id_user`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `billets`
--
ALTER TABLE `billets`
MODIFY `id_billet` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
